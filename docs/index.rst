.. ESGF-ISSUE-CLIENT documentation master file, created by
   sphinx-quickstart on Fri Sep 30 16:07:57 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to ESGF-ISSUE-CLIENT's documentation!
=============================================

Contents:

.. toctree::
   :maxdepth: 2

   synopsis
   installation
   configuration
   usage
   faq
   credits
   log


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

